﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BOSSApplication.Startup))]
namespace BOSSApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
