﻿using BOSSApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Owin;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace BOSSApplication.Controllers
{
    [Authorize(Roles = "User")]
    public class UserController : Controller
    {
        private BOSSDatabaseEntities context = new BOSSDatabaseEntities();

        // GET: User
        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
            var model = new UsersViewModel();
            var user = this.context.AspNetUsers.Find(userId);
            model.Email = user.Email;
            model.UserName = user.UserName;
            model.Money = user.Money;
            model.Socks = user.Socks;
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(int? txtDeposit, int? txtBuy, int? txtSell, string sockPrice)
        {
            var user = this.context.AspNetUsers.Find(User.Identity.GetUserId());

            if(txtDeposit != null)
            {
                user.Money += txtDeposit;
                this.context.SaveChanges();
            }

            if (sockPrice != null)
            {
                double doubleSockPrice = Convert.ToDouble(sockPrice.Replace('.',','));

                if(txtBuy != null)
                {
                    var boughtSocks = doubleSockPrice * txtBuy;
                    user.Money -= boughtSocks;
                    user.Socks += txtBuy;
                    this.context.SaveChanges();
                }

                if(txtSell != null)
                {
                    if (txtSell <= user.Socks)
                    {
                        var soldSocks = doubleSockPrice * txtSell;
                        user.Money += soldSocks;
                        user.Socks -= txtSell;
                        this.context.SaveChanges();
                    }

                    else
                    {
                        return RedirectToAction("Index", "User");
                    }
                }

            }
            return RedirectToAction("Index", "User");
        }
    }
}