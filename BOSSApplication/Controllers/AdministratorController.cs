﻿using BOSSApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BOSSApplication.Controllers
{
    [Authorize(Roles="Administrator")]
    public class AdministratorController : Controller
    {
        private BOSSDatabaseEntities context = new BOSSDatabaseEntities();
        // GET: Administrator
        public ActionResult Index()
        {
            var model = (
                from user in this.context.AspNetUsers
                from role in user.AspNetRoles
                join userRole in this.context.AspNetRoles on role.Id equals userRole.Id
                where userRole.Name == "User"
                select new UsersViewModel()
                {
                    Id = user.Id,
                    Email = user.Email,
                    UserName = user.UserName
                }
                ).ToList();
            return View(model);
        }

        public ActionResult Delete(string id)
        {
            var userId = this.context.AspNetUsers.Find(id);
            this.context.AspNetUsers.Remove(userId);
            this.context.SaveChanges();
            return RedirectToAction("Index", "Administrator");
        }
    }
}