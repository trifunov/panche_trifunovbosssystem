﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BOSSApplication.Models
{
    public class UsersViewModel
    {
        public string Id { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

        public double? Money { get; set; }

        public int? Socks { get; set; }
    }
}